<?php

namespace App\Http\Controllers;

use App\Task;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TaskController extends Controller
{
    
     public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        if($request->has('type')){
            $tasks = Task::where(['user_id' => Auth::user()->id])->where('type', '=', $request->get('type'))->get();
        }else{
            $tasks = Task::where(['user_id' => Auth::user()->id])->get();
        }

        return response()->json([
            'tasks'    => $tasks,
            'asdf'      => $request->input('type')
        ], 200);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
      public function store(Request $request)
    {
        $this->validate($request, [
            'name'          => 'required|max:255',
            'description'   => 'required',
            'file'          => 'required'
        ]);

        $fileData = $request->get('file');

        $fileName = time() . '_' . uniqid() . '.' . explode('/', explode(':', substr($fileData, 0, strpos($fileData, ';')))[1])[1];
        
        $type = explode('/', explode(':', substr($fileData, 0, strpos($fileData, ';')))[1])[1];

        $fileData = explode(',', $fileData)[1];
        $fileData = base64_decode($fileData);

        file_put_contents(public_path('files/') . $fileName, $fileData);

        $task = Task::create([
            'name'        => request('name'),
            'pdf'         => $fileName,
            'type'        => $type,
            'description' => request('description'),
            'user_id'     => Auth::user()->id
        ]);

        return response()->json([
            'task'    => $task,
            'message' => 'Success'
        ], 200);
    }
    /**
     * Display the specified resource.
     *
     * @param  \App\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function show(Task $task)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function edit(Task $task)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Task $task)
    {
        $this->validate($request, [
            'name'          => 'required|max:255',
            'description'   => 'required'
        ]);

        $fileData = $request->get('file');
        if($fileData){
            $fileName = time() . '_' . uniqid() . '.' . explode('/', explode(':', substr($fileData, 0, strpos($fileData, ';')))[1])[1];

            $type = $fileName;

            $fileData = explode(',', $fileData)[1];
            $fileData = base64_decode($fileData);

            file_put_contents(public_path('files/') . $fileName, $fileData);

            $task->type         = $type;
            $task->pdf          = $fileName;
        }

        $task->name = request('name');
        $task->description = request('description');
        $task->save();

        return response()->json([
            'message'   => 'Task updated successfully!',
            'task'      => $task
        ], 200);
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function destroy(Task $task)
    {
        $task->delete();
    }
}
